## Libre Space Foundation Templates

This repository contains the templates for freecad drawings and assembly guides, kicad templates, etc. .

## License

Licensed under the [Creative Commons, Attribution-ShareAlike 4.0 International](LICENSE) or as referenced.
